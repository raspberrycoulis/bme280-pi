#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bme280
import json
import sys
import time
import datetime

import gspread
from oauth2client.service_account import ServiceAccountCredentials
from envirophat import light, weather

GDOCS_OAUTH_JSON       = 'drive-auth.json'

# Google Docs spreadsheet name.
GDOCS_SPREADSHEET_NAME = 'PiData'

# How long to wait (in seconds) between measurements.
FREQUENCY_SECONDS      = 300    # 5 mins


def login_open_sheet(oauth_key_file, spreadsheet):
    """Connect to Google Docs spreadsheet and return the first worksheet."""
    try:
        scope =  ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(oauth_key_file, scope)
        gc = gspread.authorize(credentials)
        worksheet = gc.open(spreadsheet).sheet1
        return worksheet
    except Exception as ex:
        print('Unable to login and get spreadsheet.  Check OAuth credentials, spreadsheet name, and make sure spreadsheet is shared to the client_email address in the OAuth .json file!')
        print('Google sheet login failed with error:', ex)
        sys.exit(1)


print('Logging sensor measurements to {0} every {1} seconds.'.format(GDOCS_SPREADSHEET_NAME, FREQUENCY_SECONDS))
print('Press Ctrl-C to quit.')
worksheet = None
while True:
    # Login if necessary.
    if worksheet is None:
        worksheet = login_open_sheet(GDOCS_OAUTH_JSON, GDOCS_SPREADSHEET_NAME)

    # Attempt to get sensor reading.
    temperature, pressure, humidity = bme280.readBME280All()
    temperature = (round(temperature,1))
    pressure = (round(pressure,0))
    humidity = (round(humidity,0))

    # Output to the terminal - uncomment to show, leave if running headless
    #print ("Temp={0:.1f}*C   Pressure={1:.0f} hPa   Humidity={2:.0f} %".format(temperature, pressure, humidity))

    # Append the data in the spreadsheet, including a timestamp
    try:
        worksheet.append_row((temperature, pressure, humidity))

    except:
        # Error appending data, most likely because credentials are stale.
        # Null out the worksheet so a login is performed at the top of the loop.
        print('Append error, logging in again')
        worksheet = None
        time.sleep(FREQUENCY_SECONDS)
        continue

    # Wait 30 seconds before continuing
    #print('Wrote a row to {0}'.format(GDOCS_SPREADSHEET_NAME))
    time.sleep(FREQUENCY_SECONDS)