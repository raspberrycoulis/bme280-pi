#!/usr/bin/python
# -*- coding: utf-8 -*-

import bme280
import time
import sys

try:
    while True:
        temperature,pressure,humidity = bme280.readBME280All()
        #temperature = temperature - 1.1     # To compensate for the Pi's heat output - uncomment if attached close to Pi
        sys.stdout.write("\rTemperature is %d°C | Pressure is %dhPa | Humidity is %d%% \033[K" % (temperature,pressure,humidity))
        sys.stdout.flush()
        time.sleep(0.5)

except KeyboardInterrupt:
    print "\r"
    pass
