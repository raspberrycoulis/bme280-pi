#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This script will pull data from the BME280 sensor and send it to Beebotte. #

import time
import bme280
from beebotte import *

### Replace CHANNEL_TOKEN with that of your Beebotte channel
### and YOUR_CHANNEL_NAME_HERE with the channel you create
bbt = BBT(token = 'token_We6gDKtTGs3wu77p')
chanName = "LoungeBME280"

### Change Beebotte channel name as suits you - in this instance, it is called BME280.
temp_resource   = Resource(bbt, chanName, 'temperature')
pressure_resource  = Resource(bbt, chanName, 'pressure')
humidity_resource = Resource(bbt, chanName, 'humidity')

# Define the time between sending data to Beebotte - default is 900 seconds (15 mins)
period = 900

def run():
  while True:
    # Get the first reading from the BME280 sensor
    temperature,pressure,humidity = bme280.readBME280All()
    # Compensate for the Pi's heat output by subtracting 1.1 degrees - uncomment if BME280 is attached close to Pi
    #temperature = temperature - 1.1
    if temperature is not None and pressure is not None and humidity is not None:
        # Uncomment out to print to terminal - useful when testing
        #print("\rTemperature is %d°C | Pressure is %dhPa | Humidity is %d%% \033[K" % (temperature,pressure,humidity))
        try:
            # Send data to Beebotte
            temp_resource.write(round(temperature,1))
            pressure_resource.write(round(pressure,0))
            humidity_resource.write(round(humidity,0))
        except Exception:
          # Process exception here
          print ("Error while writing to Beebotte")
    else:
        print ("Failed to get reading. Try again!")

    # Sleep some time
    time.sleep(period)

run()
